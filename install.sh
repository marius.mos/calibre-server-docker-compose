#!/bin/bash

if [ -e .env ]; then
    source .env
else 
    echo "Please set up your .env file before starting your enviornment."
    exit 1
fi

docker network create $NETWORK

docker-compose pull

if [ -z ${SERVICE_NETWORK+X} ]; then
    docker-compose up -d
else
    docker-compose -f docker-compose-multiple-networks.yml up -d
fi