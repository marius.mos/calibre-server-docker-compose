# Calibre Server
The goal of this repository is to allow one to launch a Calibre server and web frontend.

## Dependencies

1. docker (https://docs.docker.com/engine/installation/)
2. docker-compose (https://docs.docker.com/compose/install/)

Optional:

You will likely want a reverse-proxy setup.
Such as, [nginx-proxy-companion](https://github.com/JrCs/docker-letsencrypt-nginx-proxy-companion)

## How to use it
 
1. Clone this repository:  
`git clone git@gitlab.com:aaronkjones/calibre-server-docker-compose.git`  

2. Make a copy of our `.env.sample` and rename it to `.env`:  
`cp .env.sample .env`  

3. Modify the .env file with your settings  

4. Launch the containers  
`./install.sh`  

You should then be able to reach your Calibre web instance at the URL you configured.
